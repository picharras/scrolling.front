# scrolling
Scrolling is a tiny project built in Vue which implements a simple infinite scrolling for loading images from an API

### Project setup
In order to run locally this project, you'll need to install the following software:

- Nodejs 14.15.x
- Git

Once you have installed nodejs and git, clone this project:
```
git clone git@gitlab.com:picharras/scrolling.front.git
```

Then go to the project and install the dependencies:
```
npm install
```

### Compiles and hot-reloads for development
Before you can run the project it's necessary to set the environment variables, you can find them inside the file `env.example`. You only should to create a copy named `.env` and set other values if you desire. Then run:
```
npm run serve
```

### Compiles and minifies for production
This command created the final filas used in production
```
npm run build
```

### Deploy in AWS
The project is almost ready to deploy in AWS using gitlab CI/CD. The AWS services used are AWS S3 for storing the assets generated during the build and AWS CloudFront as an CDN. Please, review the follogin variables before:

- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_CLOUD_FRONT_ID
- AWS_DEFAULT_REGION
- AWS_BUCKET

Finally check your file `.gitlab-ci.yml` and set correctly the variables `VUE_APP_API_TOKEN` and `VUE_APP_API_BASE_URL`. Once all those changes are ready, the auto deploy will be available to run each time the branch `master` is updated, you should go to the CI/CD section and run manually the task `deployToProduction`.